import { Component, OnInit } from '@angular/core';
import { ClientesService } from '../services/clientes.service';

@Component({
  selector: 'app-ver-clientes',
  templateUrl: './ver-clientes.component.html',
  styleUrls: ['./ver-clientes.component.css']
})
export class VerClientesComponent implements OnInit {
	constructor(private clientesService: ClientesService) { }

  	ngOnInit() {
  	}

  	get clientes() {
		return this.clientesService.clientes;
  	}
}
