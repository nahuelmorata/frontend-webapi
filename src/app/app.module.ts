import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { VerClientesComponent } from './ver-clientes/ver-clientes.component';
import { AgregarClienteComponent } from './agregar-cliente/agregar-cliente.component';

@NgModule({
  declarations: [
    AppComponent,
    AgregarClienteComponent,
    VerClientesComponent
  ],
  imports: [
    BrowserModule,
	AppRoutingModule,
	FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
