import { Component, OnInit } from '@angular/core';
import { ClientesService } from '../services/clientes.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-agregar-cliente',
  templateUrl: './agregar-cliente.component.html',
  styleUrls: ['./agregar-cliente.component.css']
})
export class AgregarClienteComponent implements OnInit {
	nombre: string;
	apellido: string;
	direccion: string;

	constructor(private clientesService: ClientesService, private router: Router) { }

	ngOnInit() {
	}

	enviar() {
		this.clientesService.crearCliente(this.nombre, this.apellido, this.direccion).then((_) => {
			this.nombre = "";
			this.apellido = "";
			this.direccion = "";
			this.router.navigateByUrl('/ver');
		});
	}
}
