import { Injectable } from '@angular/core';
import { BackendService } from './backend.service';
import { Cliente } from '../clases/cliente';

@Injectable({
	providedIn: 'root'
})
export class ClientesService {
	clientes: Cliente[] = [];

	constructor(private backend: BackendService) {
		this.backend.clientes.then((clientes) => {
			console.log(clientes);
			clientes.forEach((cliente) => {
				this.clientes.push(new Cliente(cliente.Nombre, cliente.Apellido, cliente.Direccion));
			});
		});
	}

	crearCliente(nombre: string, apellido: string, direccion: string): Promise<any> {
		return this.backend.crearCliente(nombre, apellido, direccion).then((_) => {
			this.clientes.push(new Cliente(nombre, apellido, direccion));
		});
	}
}
