import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgregarClienteComponent } from './agregar-cliente/agregar-cliente.component';
import { VerClientesComponent } from './ver-clientes/ver-clientes.component';


const routes: Routes = [
	{
		path: 'agregar',
		component: AgregarClienteComponent
	},
	{
		path: 'ver',
		component: VerClientesComponent
	}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
