import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class BackendService {
	private urlClientes: string = "http://localhost:5000/clientes";
	constructor() { }

	get clientes(): Promise<any> {
		return fetch(this.urlClientes, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then((response: Response) => {
			return response.json();
		});
	}

	crearCliente(nombre: string, apellido: string, direccion: string): Promise<any> {
		return fetch(this.urlClientes, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				'nombre': nombre,
				'apellido': apellido,
				'direccion': direccion
			})
		});
	}
}
